import { OnInit, Injector } from '@angular/core';
import { HttpProviderInterface, CacheProviderInterface } from './test.module';
export declare class TestComponent implements OnInit {
    cacheService: CacheProviderInterface;
    httpService: HttpProviderInterface;
    constructor(injector: Injector);
    ngOnInit(): void;
}
