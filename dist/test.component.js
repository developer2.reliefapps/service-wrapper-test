var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injector } from '@angular/core';
import { CacheServiceWrapper } from './services/cache-service-wrapper';
import { HttpServiceWrapper } from './services/http-service-wrapper';
var TestComponent = (function () {
    function TestComponent(injector) {
        this.cacheService = injector.get(CacheServiceWrapper);
        this.httpService = injector.get(HttpServiceWrapper);
        console.log('this.httpService', this.httpService);
    }
    TestComponent.prototype.ngOnInit = function () {
        this.httpService.get();
    };
    return TestComponent;
}());
TestComponent = __decorate([
    Component({
        selector: 'app-test',
        templateUrl: './test.component.html',
        styleUrls: ['./test.component.css']
    }),
    __metadata("design:paramtypes", [Injector])
], TestComponent);
export { TestComponent };
//# sourceMappingURL=test.component.js.map