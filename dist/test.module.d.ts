import { Type } from '@angular/core';
export interface ProviderInterface {
}
export interface HttpProviderInterface extends ProviderInterface {
    get(): any;
    post(): any;
    put(): any;
    delete(): any;
}
export interface CacheProviderInterface extends ProviderInterface {
    set(key: string, value: string): any;
    get(key: string): any;
    pop(key: string): any;
    clear(): any;
}
/**
 * This class define the object to inject into the module
 */
export interface ProviderDefinitionClass<T> {
    name: string;
    constructorFunction: Type<T>;
    deps: Array<any>;
    value?: any;
}
/**
 * Add required providers to the modules providers
 * @param externalProviders
 */
export declare function injectProviders(externalProviders: Array<ProviderDefinitionClass<any>>): ProviderDefinitionClass<any>[];
export declare class TestModule {
    static externalProviders: Array<any>;
}
