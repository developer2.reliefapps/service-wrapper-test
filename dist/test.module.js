var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestComponent } from './test.component';
import { HttpServiceWrapper } from './services/http-service-wrapper';
import { CacheServiceWrapper } from './services/cache-service-wrapper';
/**
 * Add required providers to the modules providers
 * @param externalProviders
 */
export function injectProviders(externalProviders) {
    externalProviders.forEach(function (provider) {
        provider.value = Object.create(provider.constructorFunction.prototype);
        provider.value.constructor.apply(provider.value, provider.deps);
    });
    return TestModule.externalProviders = externalProviders;
}
/**
 * Instantiate a provider by its name
 * @param provider
 */
function servicesFactory(provider) {
    var index = TestModule.externalProviders.findIndex(function (item) { return item.name === provider; });
    if (index >= -1) {
        console.log('LOADING ...', provider, TestModule.externalProviders[index].value);
        return TestModule.externalProviders[index].value;
    }
    throw new Error('Impossible to locate the provider');
}
var TestModule = (function () {
    function TestModule() {
    }
    return TestModule;
}());
TestModule = __decorate([
    NgModule({
        imports: [
            CommonModule
        ],
        exports: [
            TestComponent
        ],
        declarations: [TestComponent],
        providers: [
            { provide: 'http', useValue: 'http' },
            { provide: 'cache', useValue: 'cache' },
            { provide: HttpServiceWrapper, useFactory: servicesFactory, deps: ['http'] },
            { provide: CacheServiceWrapper, useFactory: servicesFactory, deps: ['cache'] }
        ]
    })
], TestModule);
export { TestModule };
//# sourceMappingURL=test.module.js.map