import { Component, OnInit, Injector } from '@angular/core';
import { HttpProviderInterface, CacheProviderInterface } from './test.module';
import { CacheServiceWrapper } from './services/cache-service-wrapper';
import { HttpServiceWrapper } from './services/http-service-wrapper';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  cacheService: CacheProviderInterface;
  httpService: HttpProviderInterface;

  constructor(
    injector: Injector
  ) {
    this.cacheService = <CacheProviderInterface> injector.get(CacheServiceWrapper);
    this.httpService = <HttpProviderInterface> injector.get(HttpServiceWrapper);
    console.log('this.httpService', this.httpService);
  }

  ngOnInit() {
    this.httpService.get();
  }

}
