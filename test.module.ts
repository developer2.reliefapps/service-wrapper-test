import { NgModule, Injectable, Injector, Type } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestComponent } from './test.component';

import { HttpServiceWrapper } from './services/http-service-wrapper';
import { CacheServiceWrapper } from './services/cache-service-wrapper';

export interface ProviderInterface {}

export interface HttpProviderInterface extends ProviderInterface {
  get();
  post();
  put();
  delete();
}

export interface CacheProviderInterface extends ProviderInterface {
  set(key: string, value: string);
  get(key: string);
  pop(key: string);
  clear();
}

/**
 * This class define the object to inject into the module
 */
export interface ProviderDefinitionClass <T> {
  name: string;
  constructorFunction: Type<T>;
  deps: Array<any>;
  value?: any;
}

/**
 * Add required providers to the modules providers
 * @param externalProviders
 */
export function injectProviders(externalProviders: Array<ProviderDefinitionClass<any>>) {

  externalProviders.forEach( provider => {
    provider.value = Object.create(provider.constructorFunction.prototype);
    provider.value.constructor.apply(provider.value, provider.deps);
  });

  return TestModule.externalProviders = externalProviders;
}

/**
 * Instantiate a provider by its name
 * @param provider
 */
function servicesFactory(provider: string) {

  const index = TestModule.externalProviders.findIndex( item => item.name === provider);

  if (index >= -1) {
    console.log('LOADING ...', provider, TestModule.externalProviders[index].value);
    return TestModule.externalProviders[index].value;
  }

  throw new Error('Impossible to locate the provider');
}

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    TestComponent
  ],
  declarations: [TestComponent],
  providers: [
    {provide: 'http', useValue: 'http'},
    {provide: 'cache', useValue: 'cache'},
    {provide: HttpServiceWrapper, useFactory: servicesFactory, deps: ['http']},
    {provide: CacheServiceWrapper, useFactory: servicesFactory, deps: ['cache']}
  ]
})
export class TestModule {
  static externalProviders: Array<any>;
}
